# Smart Referer Default Whitelists

## Guidelines for inclusion into the main default whitelist

Rules may be placed into the main default whitelist if their absence causes any
important part of a site's experience to disappear, appear broken or become
dysfunctional and

 1. the different domains used by such rule are directly owned by the same
    entity (like Google stuff imported on YouTube) – this explicitely does
    not include corporate groups that happen to include, for instance, both
    an ad service and a useful site – or
 2. a specific (i.e.: without including a global-wildcard `*` rule for either
    domain) rule can be crafted between the affected domain and the domain of
    the 3rd-party service or
 3. *in case of demonstrated major breakage on several sites, where breakage on 
    many other unknown sites is to be expected, only*, a non-specific rule
    towards the domain of the used 3rd-party service.

Also, in general, the domain of the referenced service *should not belong to
any privacy violating service*. Where „privacy violating service“ is defined
as any service offered by an entity that is either reputable of not respecting
user's privacy or may reasonability be expected to engage in such behaviour
in the future due to „user is the product“-style business practices (including
most social media sites and ad networks). Exceptions to this rule may be
allowed on a case by case basis if deemed unavoidable due to major breakage
to the site being present otherwise and if the rules falls into either
category 1 or 2 of the above list; *rules added according to category 3* of
the above list *may never include a privacy violating service*.

Specific rules must always be prefered over non-specifc ones. The need for
adding a rule must be demonstrated up-front and should be verified
(if feasable) by the person considering its inclusion. All rules should be
added with a short description of the issue they are trying to solve, as well
as the time of last verification.

When considering a new rule the person considering its inclusion should also
keep in mind that rules added to this list will be applied by default in most
installations of Smart Referer.

## Guidelines for inclusion into the Extended Whitelist

> *Note*: This does not exist yet.

Rules may be placed into the extended whitelist if

 * they are not suitable for the main default whitelist for any reason and
 * their absence causes any visible breakage on the given site and
 * their inclusion does not add a domain of a privacy violating service
   (see the definition above) into any part of the rule (neither source
   nor destination).

As with rules in the main default whitelist, specific rules must always be
prefered over non-specifc ones. The need for adding a rule must be demonstrated
up-front and should be verified (if feasable) by the person considering its
inclusion. All rules should be added with a short description of the issue they
are trying to solve, as well as the time of last verification.

Examples of use-cases for this list include: External font services, minor
images on a site, privacy-preserving advertising, …